<?php

namespace models;


require_once(__DIR__ . '/../abstracts/ITranslatable.php');
require_once(__DIR__ . '/../models/LiteratorRu.php');
require_once(__DIR__ . '/../models/LiteratorEn.php');

use abstracts\ITranslatable;
use Exception;

/**
 * Class Number
 * @package models
 */
class Number implements ITranslatable
{
    public $lang = 'ru';

    /**
     * @var \abstracts\Literator || null
     */
    private $_literator = null;

    private $_parts = [
        'millions' => 0,
        'thousands' => 0,
        'units' => 0
    ];

    public function __construct($number)
    {
        if (!is_numeric($number) || strlen($number) !== strlen(intval($number)) || $number < 0 || $number > 999999999)
            throw new Exception('Invalid params');

        $parts = str_split(sprintf('%09d', $number), 3);

        list($this->_parts['millions'],
            $this->_parts['thousands'],
            $this->_parts['units']) = array_map('intval', $parts);
    }

    public function chooseLang($lang)
    {
        if ($lang !== null)
            $this->lang = $lang;

        $literatorClassName = '\\models\\Literator' . ucfirst($this->lang);

        $this->_literator = new $literatorClassName($this);
    }

    /**
     * @param null $lang
     * @return string
     */
    public function toLiterals($lang = null)
    {
        $this->chooseLang($lang);

        foreach ($this->_parts as $name => $value) {
            $this->_literator->partToLiterals($name, $value);
        }

        return trim(implode(' ', $this->_literator->stream));
    }
}