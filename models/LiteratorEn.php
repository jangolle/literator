<?php

namespace models;


require_once(__DIR__ . '/../abstracts/Literator.php');

use abstracts\Literator;

class LiteratorEn extends Literator
{
    protected static $defaultPerson = 'n';

    /**
     * @param $partName
     * @param $value
     * @return bool || string
     */
    public function getDeclension($partName, $value)
    {
        if ($partName === 'units')
            return false;

        return $this->_literalMap['measures'][$partName][1];
    }
}