<?php

namespace models;

require_once(__DIR__ . '/../abstracts/Literator.php');

use abstracts\Literator;

class LiteratorRu extends Literator
{
    protected static $defaultPerson = 'm';

    /**
     * @param $partName
     * @param $value
     * @return bool || string
     */
    public function getDeclension($partName, $value)
    {
        if ($partName === 'units')
            return false;

        $value %= 100;

        if ($value > 10 && $value < 20)
            return $this->_literalMap['measures'][$partName][3];

        $value %= 10;

        if ($value > 1 && $value < 5)
            return $this->_literalMap['measures'][$partName][2];

        if ($value === 1)
            return $this->_literalMap['measures'][$partName][1];

        return $this->_literalMap['measures'][$partName][3];
    }
}