<?php

require_once(__DIR__ . '/models/Number.php');

use models\Number;

if (!isset($_POST['number']))
    exit;

try {
    $number = new Number($_POST['number']);

    echo 'Ru:' . $number->toLiterals('ru') . '<br>';
    echo 'En:' . $number->toLiterals('en');

} catch (Exception $e) {

    echo $e->getMessage();

}
