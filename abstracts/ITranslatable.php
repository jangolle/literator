<?php

namespace abstracts;


interface ITranslatable
{
    public function toLiterals($lang = null);

    public function chooseLang($lang);
}