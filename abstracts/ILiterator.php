<?php

namespace abstracts;


interface ILiterator
{
    public function partToLiterals($partName, $value);

    public function getDeclension($partName, $value);
}