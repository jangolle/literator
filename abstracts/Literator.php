<?php

namespace abstracts;


require_once('ILiterator.php');

abstract class Literator implements ILiterator
{
    const KEY_MASCULINE = 'm';
    const KEY_FEMININE = 'f';
    const KEY_NEUTER = 'n';

    protected $_literalMap;

    protected static $defaultPerson;

    public $stream = [];

    public function __construct(ITranslatable $number)
    {
        $this->_literalMap = require_once(__DIR__ . '/../translations/' . $number->lang . '/numerals.php');
    }

    public function partToLiterals($partName, $value)
    {
        if ($value === 0)
            if ($partName === 'units' && empty($this->stream))
                $this->stream[] = $this->_literalMap['numbers']['zero'];
            else
                return false;

        $result = [];

        $person = array_key_exists($partName, $this->_literalMap['measures']) ? $this->_literalMap['measures'][$partName][0] : static::$defaultPerson;

        $parts = str_split(sprintf('%03d', $value), 1);

        list($hundreds, $tens, $units) = array_map('intval', $parts);

        $result[] = $this->_literalMap['numbers']['hundreds'][$hundreds];

        if ($tens >= 2)
            $result[] = $this->_literalMap['numbers']['tens'][$tens] . ' ' . $this->_literalMap['numbers']['belowTen'][$person][$units];
        else
            $result[] = $tens == 1 ? $this->_literalMap['numbers']['belowTwenty'][$units] : $this->_literalMap['numbers']['belowTen'][$person][$units];

        $result[] = $this->getDeclension($partName, $value);

        $this->stream[] = implode(' ', $result);
    }
}