<?php

return [
    'numbers' => [
        'zero' => 'zero',
        'belowTen' => [
            'n' => ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'],
        ],
        'belowTwenty' => [
            'ten',
            'eleven',
            'twelve',
            'thirteen',
            'fourteen',
            'fifteen',
            'sixteen',
            'seventeen',
            'eighteen',
            'nineteen'
        ],
        'tens' => [
            '',
            '',
            'twenty',
            'thirty',
            'forty',
            'fifty',
            'sixty',
            'seventy',
            'eighty',
            'ninty'
        ],
        'hundreds' => [
            '',
            'one hundread',
            'two hundread',
            'three hundread',
            'four hundread',
            'five hundread',
            'six hundread',
            'seven hundread',
            'eight hundread',
            'nine hundread'
        ],
    ],
    'measures' => [
        'millions' => ['n', 'million'],
        'thousands' => ['n', 'thousand'],
    ]
];